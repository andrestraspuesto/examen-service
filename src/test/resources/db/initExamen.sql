insert into examen (id, nombre, n_preguntas, porcentaje_aciertos) values (1, 'examen 1', 5, 50.0);
insert into pregunta (id, examen_id, enunciado, orden)	 values (1, 1, 'pregunta 1', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (2, 1, 'pregunta 2', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (3, 1, 'pregunta 3', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (4, 1, 'pregunta 4', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (5, 1, 'pregunta 5', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (6, 1, 'pregunta 6', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (7, 1, 'pregunta 7', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (8, 1, 'pregunta 8', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (9, 1, 'pregunta 9', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (10, 1, 'pregunta 10', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (11, 1, 'pregunta 11', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (12, 1, 'pregunta 12', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (13, 1, 'pregunta 13', 1);	
insert into pregunta (id, examen_id, enunciado, orden)	 values (14, 1, 'pregunta 14', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (15, 1, 'pregunta 15', 1);
insert into pregunta (id, examen_id, enunciado, orden)	 values (16, 1, 'pregunta 16', 1);

insert into respuesta (id, pregunta_id, texto, correcta) values (1, 2, 'respuesta 1', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (2, 2, 'respuesta 2', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (3, 2, 'respuesta 3', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (4, 2, 'respuesta 4', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (5, 2, 'respuesta 5', 0);	
insert into respuesta (id, pregunta_id, texto, correcta) values (6, 2, 'respuesta 6', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (30, 1, 'respuesta 7', 0);

insert into respuesta (id, pregunta_id, texto, correcta) values (7, 6, 'respuesta 1', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (8, 6, 'respuesta 2', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (9, 6, 'respuesta 3', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (10, 6, 'respuesta 4', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (11, 6, 'respuesta 5', 0);	
insert into respuesta (id, pregunta_id, texto, correcta) values (31, 6, 'respuesta 6', 0);

insert into respuesta (id, pregunta_id, texto, correcta) values (12, 8, 'respuesta 1', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (13, 8, 'respuesta 2', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (14, 8, 'respuesta 3', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (15, 8, 'respuesta 4', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (16, 8, 'respuesta 5', 0);	
insert into respuesta (id, pregunta_id, texto, correcta) values (17, 8, 'respuesta 6', 0);

insert into respuesta (id, pregunta_id, texto, correcta) values (18, 11, 'respuesta 1', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (19, 11, 'respuesta 2', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (20, 11, 'respuesta 3', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (21, 11, 'respuesta 4', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (22, 11, 'respuesta 5', 0);	
insert into respuesta (id, pregunta_id, texto, correcta) values (23, 11, 'respuesta 6', 0);

insert into respuesta (id, pregunta_id, texto, correcta) values (24, 16, 'respuesta 1', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (25, 16, 'respuesta 2', 1);
insert into respuesta (id, pregunta_id, texto, correcta) values (26, 16, 'respuesta 3', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (27, 16, 'respuesta 4', 0);
insert into respuesta (id, pregunta_id, texto, correcta) values (28, 16, 'respuesta 5', 0);	
insert into respuesta (id, pregunta_id, texto, correcta) values (29, 16, 'respuesta 6', 0);	


insert into examen_alumno (id, examen_id, alumno_id) values (1, 1, 1);
insert into pregunta_alumno (id, examen_alumno_id, pregunta_id)	 values (1, 1, 2);
insert into pregunta_alumno (id, examen_alumno_id, pregunta_id)	 values (2, 1, 6);
insert into pregunta_alumno (id, examen_alumno_id, pregunta_id)	 values (3, 1, 8);
insert into pregunta_alumno (id, examen_alumno_id, pregunta_id)	 values (4, 1, 11);
insert into pregunta_alumno (id, examen_alumno_id, pregunta_id)	 values (5, 1, 16);	

update  pregunta_alumno set respuesta_id = 1 where id = 1;
update  pregunta_alumno set respuesta_id = 7 where id = 2;
update  pregunta_alumno set respuesta_id = 12 where id = 3;
update  pregunta_alumno set respuesta_id = 20 where id = 4;

commit;
