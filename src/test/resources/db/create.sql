create or replace table examen (
	id int auto_increment primary key,
	nombre varchar(256),
	n_preguntas int default 10, 
	porcentaje_aciertos float default 5.0,
	borrado bit(1) default 0
);

create or replace table pregunta (
	id int auto_increment primary key,
	examen_id int, 
	enunciado varchar(256), 
	orden int,
	borrado bit(1) default 0
);

create or replace table respuesta (
	id int auto_increment primary key,
	pregunta_id int,
	texto varchar(256),
	correcta bit(1),
	borrado bit(1) default 0
);

create or replace table examen_alumno(
	id int auto_increment primary key,
	examen_id int,
	alumno_id int,
	n_aciertos int default 0,
	n_fallos int default 0,
	corregido bit(1) default 0,
	borrado bit(1) default 0
);

create or replace table pregunta_alumno(
	id int auto_increment primary key,
	examen_alumno_id int,
	pregunta_id int,
	respuesta_id int,
	borrado bit(1) default 0
);

ALTER TABLE pregunta ADD CONSTRAINT fk_preg_exa FOREIGN KEY (examen_id) REFERENCES examen(id) ON DELETE cascade ON UPDATE cascade;
ALTER TABLE respuesta ADD CONSTRAINT fk_res_preg FOREIGN KEY (pregunta_id) REFERENCES pregunta(id) ON DELETE cascade ON UPDATE cascade;
ALTER TABLE examen_alumno ADD CONSTRAINT fk_ex_al_exam FOREIGN KEY (examen_id) REFERENCES examen(id) ON DELETE  NO ACTION ON UPDATE cascade;
ALTER TABLE pregunta_alumno ADD CONSTRAINT fk_preg_alu_preg FOREIGN KEY (pregunta_id) REFERENCES pregunta(id) ON DELETE  NO ACTION ON UPDATE cascade;
