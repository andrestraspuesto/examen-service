package andrestraspuesto.daos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import andrestraspuesto.config.Config;
import andrestraspuesto.domains.Examen;
import andrestraspuesto.daos.ExamenDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=Config.class)
public class ExamenDaoTest  extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Autowired
	ExamenDao examenDao;
	
	
	@Test
	public void save(){
		Examen examen = new Examen();
		examen.setNombre("Examen 1");
		examen.setNPreguntas(5);
		examen.setPorcentajeAciertos(80.0);

		examen = examenDao.save(examen);

		assertNotNull(examen.getId());

	}
	
	

}
