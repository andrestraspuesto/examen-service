
package andrestraspuesto.controller;

import andrestraspuesto.config.Config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.test.context.jdbc.Sql;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import andrestraspuesto.domains.Examen;
import andrestraspuesto.domains.Pregunta;
import andrestraspuesto.domains.Respuesta;

import andrestraspuesto.security.util.JwtTokenGenerator;
import andrestraspuesto.security.transfer.JwtUserDto;

/**
 * @author andrestraspuesto@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Config.class)
@WebAppConfiguration
public class GestionExamenControllerTest {
	
/*
documentacion de los posibles matchers http://hamcrest.org/JavaHamcrest/javadoc/1.3/org/hamcrest/Matchers.html
http://docs.spring.io/spring/docs/current/javadoc-api/index.html?org/springframework/web/bind/annotation/RequestMapping.html
*/
	
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;


    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
            .findAny()
            .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void ping() throws Exception {
        
        mockMvc.perform(get("/config-examen/ping"))
                .andExpect(status().isOk());
    }


    @Test    
    @Sql(scripts = "/db/initExamen.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/clearExamen.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void getExamen() throws Exception {
        String bearerToken = createToken(1L, 1L, "ADMIN", "administrador");
        mockMvc.perform(get("/config-examen/1/").header("Authorization", bearerToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.nombre", is("examen 1")))
                .andExpect(jsonPath("$.preguntas[15].id", is(16)))
                ;
    }

    @Test    
    public void getWrongExamen() throws Exception {
        mockMvc.perform(get("/config-examen/null/")).andExpect(status().isBadRequest());
    }

    @Test
    @Sql(scripts = "/db/clearExamen.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void createExamen() throws Exception {
        Examen examen = new Examen();
        examen.setNombre("examen de test");
        examen.setNPreguntas(10);
        examen.setPorcentajeAciertos(80.0);
        String examenJson = json(examen);
        this.mockMvc.perform(post("/config-examen")
                .contentType(contentType)
                .content(examenJson))
                .andExpect(status().isCreated());
    }

    @Test
    @Sql(scripts = "/db/initEmptyExamen.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/clearExamen.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void addPregunta() throws Exception {
        Pregunta pregunta = new Pregunta();
        pregunta.setExamenId(1L);
        pregunta.setEnunciado("pregunta de test");
        pregunta.setOrden(1);
        List<Respuesta> respuestas = new ArrayList<>();
        Respuesta r1 = new Respuesta();
        r1.setTexto("respuesta 1");
        r1.setCorrecta(true);
        respuestas.add(r1);
        r1 = new Respuesta();
        r1.setTexto("respuesta 2");
        r1.setCorrecta(false);
        respuestas.add(r1);
        pregunta.setRespuestas(respuestas);
        String preguntaJson = json(pregunta);

        this.mockMvc.perform(post("/config-examen/pregunta")
                .contentType(contentType)
                .content(preguntaJson))
                .andExpect(status().isCreated());
    }

    @Test
    @Sql(scripts = "/db/initExamenEmptyPregunta.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/db/clearExamen.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void addRespuesta() throws Exception {
        
        Respuesta r1 = new Respuesta();
        r1.setTexto("respuesta 1");
        r1.setPreguntaId(1L);
        r1.setCorrecta(true);
      
        String respuestaJson = json(r1);

        this.mockMvc.perform(post("/config-examen/respuesta")
                .contentType(contentType)
                .content(respuestaJson))
                .andExpect(status().isCreated());
    }



    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    protected String createToken(Long userId, Long cursoId, String roles, String username){
        JwtUserDto dto = new JwtUserDto();
        dto.setId(userId);
        dto.setCursoId(cursoId);
        dto.setUsername(username);
        dto.setRole(roles);
        return "Bearer " + JwtTokenGenerator.generateToken(dto, "secretojwttest");
    }
}