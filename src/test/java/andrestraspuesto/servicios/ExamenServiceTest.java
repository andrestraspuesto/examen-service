package andrestraspuesto.daos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.jdbc.Sql;
import andrestraspuesto.config.Config;
import andrestraspuesto.domains.Examen;
import andrestraspuesto.domains.ExamenAlumno;
import andrestraspuesto.domains.Pregunta;
import andrestraspuesto.servicios.ExamenService;
import andrestraspuesto.domains.PreguntaAlumno;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=Config.class)
public class ExamenServiceTest  extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Autowired
	ExamenService examenService;
	
	
	@Test
	public void create(){
		Examen examen = new Examen();
		examen.setNombre("Examen 1");
		examen.setNPreguntas(5);
		examen.setPorcentajeAciertos(80.0);
		Pregunta pregunta = new Pregunta();
		pregunta.setEnunciado("enunciado");
		pregunta.setOrden(1);
		List<Pregunta> preguntas = new ArrayList<>();
		preguntas.add(pregunta);
		examen.setPreguntas(preguntas);
		examen = examenService.saveExamen(examen);

		assertNotNull("examen tiene id",examen.getId());
		examen.getPreguntas().forEach(p -> assertNotNull("Pregunta " + p.getEnunciado() + " tiene id",p.getId()));
	}

	@Test
	@Sql(scripts = "/db/initExamen.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/db/clearExamen.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
	public void getExamenAlumno(){
		Long alumnoId = 23L;
		Long examenId = 1L;
		ExamenAlumno examen = examenService.getExamenAlumno(alumnoId, examenId);
		assertNotNull(examen);
		int expectedNPreg = 5;
		assertEquals(expectedNPreg, examen.getPreguntas().size());
	}

	@Test
	@Sql(scripts = "/db/initExamen.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/db/clearExamen.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
	public void getExamenAlumnoCuandoYaExiste(){
		Long alumnoId = 1L;
		Long examenId = 1L;
		ExamenAlumno examen = examenService.getExamenAlumno(alumnoId, examenId);
		assertNotNull(examen);
		int expectedNPreg = 5;
		assertEquals(expectedNPreg, examen.getPreguntas().size());
		final Set<Long> expectedPreguntas = new HashSet<>(Arrays.asList(new Long[]{new Long(2), new Long(6), new Long(8), new Long(11), new Long(16)}));
		examen.getPreguntas().forEach(o ->  assertTrue(expectedPreguntas.contains(o.getPregunta().getId())));
	}


	@Test
	@Sql(scripts = "/db/initExamen.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/db/clearExamen.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
	public void saveRespuestaAlumno(){
		final Long alumnoId = 1L;
		final Long examenId = 1L;
		final Long respuestaId = 1L;
		ExamenAlumno examen = examenService.saveRespuestaAlumno(alumnoId, examenId, respuestaId);
		assertNotNull(examen);
		Optional<PreguntaAlumno> pregunta = examen.getPreguntas().stream().filter(p -> p.getRespuesta().getId().equals(respuestaId)).findAny();
		assertTrue(pregunta.isPresent());
	}

	@Test(expected = andrestraspuesto.exceptions.IllegalAnswerException.class)
	@Sql(scripts = "/db/initExamen.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/db/clearExamen.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
	public void saveRespuestaAlumnoFueraDeExamenAlumno(){
		Long alumnoId = 1L;
		Long examenId = 1L;
		Long respuestaId = 30L;
		ExamenAlumno examen = examenService.saveRespuestaAlumno(alumnoId, examenId, respuestaId);
		
	}

	@Test
	@Sql(scripts = "/db/initExamen.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = "/db/clearExamen.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
	public void corrigeExamen(){
		Long alumnoId = 1L;
		Long examenId = 1L;
		Integer aciertos = 3;
		Integer fallos = 1;
		ExamenAlumno examen = examenService.corregirExamenAlumno(alumnoId, examenId);

		assertEquals(aciertos, examen.getNAciertos());
		assertEquals(fallos, examen.getNFallos());
	}

}

