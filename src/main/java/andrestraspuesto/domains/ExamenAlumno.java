package andrestraspuesto.domains;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "examen_alumno")
public class ExamenAlumno implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "examen_id")
	private Long examenId;

	@Column(name = "alumno_id")
	private Long alumnoId;

	private Integer nAciertos;

	private Integer nFallos;

	@Column(name = "corregido")
	private Boolean corregido;

	@Column(name = "borrado")
	private Boolean borrado;

	@JoinColumn(name = "examen_alumno_id")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PreguntaAlumno> preguntas;

	public void setId(Long id){ this.id = id;}
	
	public Long getId(){ return id;}

	public void setExamenId(Long examenId){ this.examenId = examenId;}
	
	public Long getExamenId(){ return examenId;}

	public void setAlumnoId(Long alumnoId){ this.alumnoId = alumnoId;}
	
	public Long getAlumnoId(){ return alumnoId;}

	public void setNFallos(Integer nFallos){ this.nFallos = nFallos;}
	
	public Integer getNFallos(){ return nFallos;}

	public void setNAciertos(Integer nAciertos){ this.nAciertos = nAciertos;}
	
	public Integer getNAciertos(){ return nAciertos;}

	public void setCorregido(Boolean corregido){ this.corregido = corregido;}
	
	public Boolean getCorregido(){ return corregido;}

	public void setBorrado(Boolean borrado){ this.borrado = borrado;}
	
	public Boolean getBorrado(){ return borrado;}

	public void setPreguntas(List<PreguntaAlumno> preguntas){ this.preguntas = preguntas;}
	
	public List<PreguntaAlumno> getPreguntas(){ return preguntas;}

}