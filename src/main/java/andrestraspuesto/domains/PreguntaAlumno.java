package andrestraspuesto.domains;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pregunta_alumno")
public class PreguntaAlumno implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;

	@Column(name = "examen_alumno_id")
	private Long examenAlumnoId;

	@JoinColumn(name = "pregunta_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
	private Pregunta pregunta;

	@JoinColumn(name = "respuesta_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
	private Respuesta respuesta;

	@Column(name = "borrado")
	private Boolean borrado;

	public void setId(Long id){ this.id = id;}
	
	public Long getId(){ return id;}

	public void setExamenAlumnoId(Long examenAlumnoId){ this.examenAlumnoId = examenAlumnoId;}
	
	public Long getExamenAlumnoId(){ return examenAlumnoId;}

	public void setPregunta(Pregunta pregunta){ this.pregunta = pregunta;}
	
	public Pregunta getPregunta(){ return pregunta;}

	public void setRespuesta(Respuesta respuesta){ this.respuesta = respuesta;}
	
	public Respuesta getRespuesta(){ return respuesta;}

	public void setBorrado(Boolean borrado){ this.borrado = borrado;}
	
	public Boolean getBorrado(){ return borrado;}

	@Override
	public String toString() {
		return String.format("preguntaAlumno: { id = %d, examenAlumnoId = %d, Pregunta = %s}", id, examenAlumnoId, pregunta.toString());
	}

	@Override
	public boolean equals(Object obj){
		boolean eq = obj != null && obj instanceof PreguntaAlumno;
		if(eq){
			PreguntaAlumno p = (PreguntaAlumno) obj;
			eq = id != null && p.id != null && id.equals(p.id);
		}
		return eq;
	}

	@Override
	public int hashCode(){
        return id.hashCode();
	}
}