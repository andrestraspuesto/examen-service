package andrestraspuesto.domains;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Pregunta implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "examen_id")
	private Long examenId;

	private String enunciado;

	private Integer orden;

	@Column(name = "borrado")
	private Boolean borrado;

	@JoinColumn(name = "pregunta_id")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Respuesta> respuestas;

	public void setId(Long id){ this.id = id;}
	
	public Long getId(){ return id;}

	public void setExamenId(Long examenId){ this.examenId = examenId;}
	
	public Long getExamenId(){ return examenId;}

	public void setEnunciado(String enunciado){ this.enunciado = enunciado;}
	
	public String getEnunciado(){ return enunciado;}

	public void setOrden(Integer orden){ this.orden = orden;}
	
	public Integer getOrden(){ return orden;}

	public void setBorrado(Boolean borrado){ this.borrado = borrado;}
	
	public Boolean getBorrado(){ return borrado;}

	public void setRespuestas(List<Respuesta> respuestas){ this.respuestas = respuestas;}
	
	public List<Respuesta> getRespuestas(){ return respuestas;}

	@Override
	public String toString() {
		return String.format("pregunta: {id = %d, examenId = %d, enunciado = %s, orden = %d}", id, examenId, enunciado, orden);
	}
}