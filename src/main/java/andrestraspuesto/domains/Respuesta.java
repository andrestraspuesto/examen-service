package andrestraspuesto.domains;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Respuesta implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "pregunta_id")
	private Long preguntaId;

	private String texto;

	private Boolean correcta;

	@Column(name = "borrado")
	private Boolean borrado;

	public void setId(Long id){ this.id = id;}
	
	public Long getId(){ return id;}

	public void setPreguntaId(Long preguntaId){ this.preguntaId = preguntaId;}
	
	public Long getPreguntaId(){ return preguntaId;}

	public void setTexto(String texto){ this.texto = texto;}
	
	public String getTexto(){ return texto;}

	public void setCorrecta(Boolean correcta){ this.correcta = correcta;}
	
	public Boolean getCorrecta(){ return correcta;}

	public void setBorrado(Boolean borrado){ this.borrado = borrado;}
	
	public Boolean getBorrado(){ return borrado;}	
}