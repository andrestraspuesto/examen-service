package andrestraspuesto.domains;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Min;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.DecimalMax;


@Entity
public class Examen implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, length = 256)
	private String nombre;

	@Column(name = "n_preguntas",nullable = false)
	private Integer nPreguntas;

	@Column(name = "porcentaje_aciertos",nullable = false)
	private Double porcentajeAciertos;

	@Column(name = "borrado")
	private Boolean borrado;

	@JoinColumn(name = "examen_id")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Pregunta> preguntas;

	public void setId(Long id){ this.id = id;}
	
	public Long getId(){ return id;}	

	public void setNombre(String nombre){ this.nombre = nombre;}
	
	public String getNombre(){ return nombre;}

	public void setNPreguntas(Integer nPreguntas){ this.nPreguntas = nPreguntas;}
	
	public Integer getNPreguntas(){ return nPreguntas;}

	public void setPorcentajeAciertos(Double porcentajeAciertos){ this.porcentajeAciertos = porcentajeAciertos;}
	
	public Double getPorcentajeAciertos(){ return porcentajeAciertos;}

	public void setBorrado(Boolean borrado){ this.borrado = borrado;}
	
	public Boolean getBorrado(){ return borrado;}

	public void setPreguntas(List<Pregunta> preguntas){ this.preguntas = preguntas;}
	
	public List<Pregunta> getPreguntas(){ return preguntas;}
}