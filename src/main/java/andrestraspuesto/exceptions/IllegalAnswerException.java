package andrestraspuesto.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IllegalAnswerException extends RuntimeException {
	
	public IllegalAnswerException(){

	}

	public IllegalAnswerException(String message){
		super(message);
	}

	public IllegalAnswerException(String message, Throwable cause){
		super(message, cause);
	}
}