package andrestraspuesto.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IncompleteExamConfigurationException extends RuntimeException {
	
	public IncompleteExamConfigurationException(){

	}

	public IncompleteExamConfigurationException(String message){
		super(message);
	}

	public IncompleteExamConfigurationException(String message, Throwable cause){
		super(message, cause);
	}
}