package andrestraspuesto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import andrestraspuesto.security.config.WebSecurityConfig;
import org.springframework.context.annotation.Import;

@Import(WebSecurityConfig.class)
@SpringBootApplication
public class ExamServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamServiceApplication.class, args);
	}
}
