package andrestraspuesto.controller;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.beans.factory.annotation.Autowired;

import andrestraspuesto.servicios.ExamenService;
import andrestraspuesto.domains.Examen;
import andrestraspuesto.domains.Pregunta;
import andrestraspuesto.domains.Respuesta;

import andrestraspuesto.exceptions.WrongRequestException;

import org.springframework.security.access.prepost.PreAuthorize;

@RestController
@RequestMapping("config-examen")
public class GestionExamenController {
	
	@Autowired
	private ExamenService examenService;

    
	@RequestMapping(value = "/ping", method=RequestMethod.GET)
    public String ping() {
        return "OK";
    }	

    @Transactional
    @RequestMapping(value = "/{id}", method=RequestMethod.GET, produces = "application/json")
    public Examen getExamen(@PathVariable("id") Long id){
    	Examen examen = null;
    	if(id != null){
    		examen = examenService.getExamen(id);
    	} else {
            throw new  WrongRequestException();
        }

    	return examen;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> saveExamen(@RequestBody Examen examen) {
        Examen result =  this.examenService.saveExamen(examen);
        return new ResponseEntity(result, HttpStatus.CREATED);    
    }

    @Transactional
    @RequestMapping(value = "/pregunta", method=RequestMethod.POST, produces = "application/json")
    ResponseEntity<?> savePregunta(@RequestBody Pregunta pregunta){
        if(pregunta == null || pregunta.getExamenId() == null){
            throw new  WrongRequestException();
        }
        return new ResponseEntity(examenService.savePregunta(pregunta), HttpStatus.CREATED);
    }

    @Transactional
    @RequestMapping(value = "/respuesta", method=RequestMethod.POST, produces = "application/json")
    ResponseEntity<?> saveRespuesta(@RequestBody Respuesta respuesta){
        if(respuesta == null || respuesta.getPreguntaId() == null){
            throw new  WrongRequestException();
        }
        return new ResponseEntity(examenService.saveRespuesta(respuesta), HttpStatus.CREATED);
    }

}