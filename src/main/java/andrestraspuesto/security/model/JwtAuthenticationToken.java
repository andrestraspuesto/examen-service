package andrestraspuesto.security.model;


import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;


/**
 * Contenedor para el token de la peticion
 *
 */
public class JwtAuthenticationToken extends UsernamePasswordAuthenticationToken {


    private String token;

    public JwtAuthenticationToken(String token) {
        super(null, null);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}