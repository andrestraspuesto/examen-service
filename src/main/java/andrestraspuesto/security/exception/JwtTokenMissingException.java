package andrestraspuesto.security.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * Se lanza cuando no se encuentra el token en la cabecera
 */

public class JwtTokenMissingException extends AuthenticationException {


    public JwtTokenMissingException(String msg) {
        super(msg);
    }
}
