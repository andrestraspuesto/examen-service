package andrestraspuesto.security.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * Se lanza cuando no se puede leer el token
 */
public class JwtTokenMalformedException extends AuthenticationException {


    public JwtTokenMalformedException(String msg) {
        super(msg);
    }
}
