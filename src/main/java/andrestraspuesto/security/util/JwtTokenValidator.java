package andrestraspuesto.security.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import andrestraspuesto.security.transfer.JwtUserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.PropertySource;

/**
 * valida el token comprobando con la clave
 *
 */
@PropertySource("classpath:application.properties")
@Component
public class JwtTokenValidator {



    @Value("${jwt.secret}")
    private String secret;

    /**
     * Intenta convertir un string en un token jwt, si tiene exito devuelve un usuario
     * @param token string 
     * @return tusuario extraido del token
     */
    public JwtUserDto parseToken(String token) {
        JwtUserDto u = null;

        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            System.out.println(body);
            u = new JwtUserDto();
            u.setUsername(body.getSubject());
            u.setId(Long.parseLong((String) body.get("userId")));
            u.setCursoId(Long.parseLong((String) body.get("cursoId")));
            u.setRole((String) body.get("role"));

        } catch (JwtException e) {
            e.printStackTrace();
        }
        return u;
    }
}
