package andrestraspuesto.security.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import andrestraspuesto.security.transfer.JwtUserDto;

/**
 * Clase para generar tokens 
 */
public class JwtTokenGenerator {

    /**
     * Genera un token que no caduca
     *
     * @param usuario para el que se genera el token
     * @return devuelve el token
     */
    public static String generateToken(JwtUserDto u, String secret) {
        Claims claims = Jwts.claims().setSubject(u.getUsername());
        claims.put("userId", u.getId() + "");
        claims.put("role", u.getRole());
        claims.put("cursoId", u.getCursoId() + "");

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

}
