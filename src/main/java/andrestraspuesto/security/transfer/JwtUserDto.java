package andrestraspuesto.security.transfer;

/**
 * Repositorio para la informacion extraida del token
 *
 */
public class JwtUserDto {

    private Long id;

    private Long cursoId;

    private String username;

    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setCursoId(Long cursoId){ this.cursoId = cursoId;}
    
    public Long getCursoId(){ return cursoId;}
}