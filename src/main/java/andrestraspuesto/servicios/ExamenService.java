package andrestraspuesto.servicios;

import andrestraspuesto.domains.Examen;
import andrestraspuesto.domains.Pregunta;
import andrestraspuesto.domains.Respuesta;
import andrestraspuesto.domains.ExamenAlumno;
import andrestraspuesto.exceptions.IncompleteExamConfigurationException;
import andrestraspuesto.exceptions.IllegalAnswerException;
import org.springframework.security.access.prepost.PreAuthorize;

public interface ExamenService {
	
	/**
	* Crea la configuracion de un examen
	* @param examen el objeto con la configuracion del examen 
	* @return el examen con id
	*/
	public Examen saveExamen(Examen examen);
	
	/**
	* Devuelve la configuracion del examen solicitado
	* @return el examen asociado 
	*/
	//@PreAuthorize("hasRole('ADMIN')")
	public Examen getExamen(Long examenId);

	/**
	* Incluye una nueva pregunta a la configuracion del examen a la que se asocie
	* @param pregunta la pregunta a incluir
	* @return la pregunta con id
	*/
	public Pregunta savePregunta(Pregunta pregunta);

	/**
	* Incluye una nueva respuesta a la pregunta asociada
	* @param respuesta la respuesta a incluir
	* @return respuesta la respuesta con id	
	*/
	public Respuesta saveRespuesta(Respuesta respuesta);

	/**
	* Obtiene el examen del alumno indicado asociado a la configuracion de examen asociada.
	* Si el alumno no tiene examen generado se le genera, si lo tiene se recupera
	* @param alumnoId identificador del alumno del que se quiere recuperar el examen
	* @param examenId identificador de la configuracion del examen
	* @return el examen del alumno
	* @throws IncompleteExamConfigurationException cuando el examen no tiene un pool de preguntas suficiente para
	* cumplir con el numero de preguntas parametrizadas, o cuando no existe
	*/
	public ExamenAlumno getExamenAlumno(Long alumnoId, Long examenId) throws IncompleteExamConfigurationException;

	/**
	* Registra la respuesta del alumno, si esta no pertence al examen del alumno lanza 
	* @param alumnoId identificador del alumno
	* @param examenId identificador de la configuracion del examen
	* @param respuestaId identificador de la respuesta escogida por el alumno
	* @return examen del alumno tras incluir la respuesta
	* @throws IllegalAnswerException cuando la respuestaId no corresponde a ninguna de las preguntas del examen del alumno
	*/
	public ExamenAlumno saveRespuestaAlumno(Long alumnoId, Long examenId, Long respuestaId) throws IllegalAnswerException ;


	/**
	* Guarda el examen del alumno, si no coincide el examen pasado como argumento con el que esta
	* registrado lanza IllegalAnswerException
	* @param examen examen que se quiere guardar
	* @return el examen del alumno que se ha guardado
	* @throws IllegalAnswerException cuando el examen almacenado no concuerda con el examen recibido como
	* argumento
	*/
	public ExamenAlumno saveExamenAlumno(ExamenAlumno examen) throws IllegalAnswerException;

	/**
	* Recupera el examen del alumno de BBDD y lo corrige
	* @param alumnoId identificador del alumno
	* @param examenId identificador de la configuracion del examen
	* @return examen corregido
	*/
	public ExamenAlumno corregirExamenAlumno(Long alumnoId, Long examenId);
}