package andrestraspuesto.servicios;


import java.time.LocalDateTime;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import andrestraspuesto.daos.ExamenDao;
import andrestraspuesto.daos.PreguntaDao;
import andrestraspuesto.daos.RespuestaDao;
import andrestraspuesto.daos.ExamenAlumnoDao;
import andrestraspuesto.daos.PreguntaAlumnoDao;

import andrestraspuesto.domains.Examen;
import andrestraspuesto.domains.Pregunta;
import andrestraspuesto.domains.Respuesta;
import andrestraspuesto.domains.ExamenAlumno;
import andrestraspuesto.domains.PreguntaAlumno;

import andrestraspuesto.exceptions.IncompleteExamConfigurationException;
import andrestraspuesto.exceptions.IllegalAnswerException;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Map;

@Service
public class ExamenServiceImpl implements ExamenService {

	@Autowired
	private ExamenDao examenDao;

	@Autowired
	private PreguntaDao preguntaDao;

	@Autowired
	private RespuestaDao respuestaDao;

	@Autowired
	private ExamenAlumnoDao examenAlumnoDao;

	@Autowired
	private PreguntaAlumnoDao preguntaAlumnoDao;
	
	@Override
	public Examen saveExamen(Examen examen){
		if(examen != null ){
			examen = examenDao.save(examen);
		}
		return examen;
	}
	
	@Override
	public Examen getExamen(Long examenId){
		Examen examen = null;
		if(examenId != null){
			Optional<Examen> opt = examenDao.findOne(examenId);
			if(opt.isPresent()){
				examen = opt.get();				
			}
		}

		return examen;
	}

	@Override
	public Pregunta savePregunta(Pregunta pregunta){
		if(pregunta != null){
			pregunta = preguntaDao.save(pregunta);
			final Long preguntaId = pregunta.getId();
			pregunta.getRespuestas().forEach(r -> r.setPreguntaId(preguntaId));
		}
		return pregunta;
	}

	@Override
	public Respuesta saveRespuesta(Respuesta respuesta){
		if(respuesta != null){
			respuesta = respuestaDao.save(respuesta);
		}

		return respuesta;
	}

	@Override
	public ExamenAlumno getExamenAlumno(Long alumnoId, Long examenId) throws IncompleteExamConfigurationException{

		ExamenAlumno examenAlumno = null;
		Optional<ExamenAlumno> queryResult = examenAlumnoDao.findByAlumnoId(alumnoId, examenId); 

		if(queryResult.isPresent()){
			examenAlumno = queryResult.get();
		} else {
			Optional<Examen> examenOpt = examenDao.findOne(examenId);
			if(!examenOpt.isPresent()){
				throw new IncompleteExamConfigurationException("The exam doesn't exist");
			}
			Examen examen = examenOpt.get();
			if(examen == null || examen.getPreguntas() == null || examen.getNPreguntas() == null || examen.getPreguntas().size() < examen.getNPreguntas()){
				throw new IncompleteExamConfigurationException("wrong number of questions stablished in the exam ");
			}
			examenAlumno = new ExamenAlumno();
			examenAlumno.setExamenId(examenId);
			examenAlumno.setAlumnoId(alumnoId);
			examenAlumno = examenAlumnoDao.save(examenAlumno);
			final Long examenAlumnoId = examenAlumno.getId();
			List<Pregunta> preguntasPool = examen.getPreguntas();
			Collections.shuffle(preguntasPool, new Random(System.nanoTime()));
			List<PreguntaAlumno> preguntas = preguntasPool.stream().limit(examen.getNPreguntas())
				.map(p -> {
					PreguntaAlumno pa = new PreguntaAlumno();
					pa.setExamenAlumnoId(examenAlumnoId);
					pa.setPregunta(p);
					preguntaAlumnoDao.save(pa);
					return pa;
				}).collect(Collectors.toList());
			examenAlumno.setPreguntas(preguntas);
			examenAlumno.setCorregido(true);
		}

		return examenAlumno;

	}

	@Override
	public ExamenAlumno saveRespuestaAlumno(Long alumnoId, Long examenId, Long respuestaId) throws IllegalAnswerException {
		if(alumnoId == null || examenId == null || respuestaId == null) {
			throw new IllegalAnswerException("null arguments are not allowed");
		}
		final ExamenAlumno examenAlumno = examenAlumnoDao.findByAlumnoId(alumnoId, examenId)
			.orElseThrow(() -> new IllegalAnswerException("examenId and alumnoId don't match"));
		final Respuesta respuesta = respuestaDao.findOne(respuestaId)
			.orElseThrow(() -> new IllegalAnswerException("respuestaId doesn't match"));

		PreguntaAlumno pregunta = examenAlumno.getPreguntas().stream().filter(p -> p.getPregunta().getId().equals(respuesta.getPreguntaId())).findAny()
			.orElseThrow(() -> new IllegalAnswerException("respuestaId doesn't fit any question"));
		
		pregunta.setRespuesta(respuesta);
		preguntaAlumnoDao.save(pregunta);

		return examenAlumno;
	}

	@Override
	public ExamenAlumno saveExamenAlumno(ExamenAlumno examen){
		if(examen != null){
			final ExamenAlumno referencia = examenAlumnoDao.findByAlumnoId(examen.getAlumnoId(), examen.getExamenId())
				.orElseThrow(() -> new IllegalAnswerException("examenId and alumnoId don't match"));
			final List<PreguntaAlumno> preguntasRef = referencia.getPreguntas();
			final List<PreguntaAlumno> preguntas = examen.getPreguntas();
			if(preguntasRef == null || preguntas == null || preguntas.size() != preguntasRef.size()){
				throw new IllegalAnswerException("sent exam and configured exam don't match");
			}
			boolean ok = true;
			for(int i = 0; i < preguntasRef.size() && ok; i++){
				PreguntaAlumno ref = preguntasRef.get(i);
				PreguntaAlumno curr = preguntas.get(i);
				ok = ref.equals(curr);
				final Long resAluId = curr.getRespuesta() != null ? curr.getRespuesta().getId(): null;
				if(resAluId != null){
					Respuesta respuesta = ref.getPregunta().getRespuestas().stream().filter(r -> r.getId().equals(resAluId)).findFirst()
						.orElseThrow(() -> new IllegalAnswerException("respuestaId doesn't fit any question"));
					curr.setRespuesta(respuesta);
				}
			}

			examen = examenAlumnoDao.save(examen);
		}

		return examen;
	}

	@Override
	public ExamenAlumno corregirExamenAlumno(Long alumnoId, Long examenId){
		if(alumnoId == null || examenId == null) {
			throw new IllegalAnswerException("null arguments are not allowed");
		}
		ExamenAlumno examen = examenAlumnoDao.findByAlumnoId(alumnoId,examenId)
			.orElseThrow(() -> new IllegalAnswerException("examenId and alumnoId don't match"));
		Map<Boolean, List<String>> correccion = examen.getPreguntas().stream().filter(p -> p.getRespuesta() != null)
			.map(p -> p.getRespuesta().getCorrecta()? "OK": "KO").collect(Collectors.partitioningBy(s -> s.equals("OK")));
		examen.setNAciertos(correccion.get(true).size());
		examen.setNFallos(correccion.get(false).size());
		if(examen != null){
			examen =examenAlumnoDao.save(examen);
		}
		return examen;
	}
}