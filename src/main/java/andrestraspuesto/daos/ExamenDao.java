package andrestraspuesto.daos;

import andrestraspuesto.domains.Examen;

public interface ExamenDao extends GenericDao<Examen, Long>{}