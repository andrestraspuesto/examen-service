package andrestraspuesto.daos;

import andrestraspuesto.domains.ExamenAlumno;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ExamenAlumnoDao extends GenericDao<ExamenAlumno, Long>{

	@Query("select ea from ExamenAlumno ea where ea.alumnoId = :alumnoId and ea.examenId = :examenId")
	public Optional<ExamenAlumno> findByAlumnoId(@Param("alumnoId") Long alumnoId, @Param("examenId") Long examenId);
}