package andrestraspuesto.daos;

import andrestraspuesto.domains.Pregunta;

public interface PreguntaDao extends GenericDao<Pregunta, Long>{}