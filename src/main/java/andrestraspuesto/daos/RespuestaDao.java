package andrestraspuesto.daos;

import andrestraspuesto.domains.Respuesta;

public interface RespuestaDao extends GenericDao<Respuesta, Long>{}