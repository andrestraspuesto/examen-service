package andrestraspuesto.daos;

import andrestraspuesto.domains.PreguntaAlumno;

public interface PreguntaAlumnoDao extends GenericDao<PreguntaAlumno, Long>{}